var io = require('socket.io')(3000);

var game = {
  colorOptions: [
    {text: 'Red' , color: 'red'},
    {text: 'Blue' , color: 'blue'},
    {text: 'Green' , color: 'green'}
  ],
  colorTroller: [
    {text: 'Red' , color: 'red'},
    {text: 'Blue' , color: 'blue'},
    {text: 'Green' , color: 'green'},
    {text: 'Green' , color: 'red'},
    {text: 'Red' , color: 'blue'},
    {text: 'Blue' , color: 'green'},
    {text: 'Blue' , color: 'red'},
    {text: 'Green' , color: 'blue'},
    {text: 'Red' , color: 'green'}
  ],
  currentColor: {},
  setColor: function(){
    console.log('COLOR TROLLER',game.colorTroller[Math.floor(Math.random() * 8)]);
    game.currentColor = game.colorTroller[Math.floor(Math.random() * 8)];
    for(var room in io.sockets.adapter.rooms){
      io.sockets.in(room).emit('newColor', game.currentColor);
    }
  },
  isCorrectColor: function(color){
    console.log('color 1', color);
    console.log('color 2', game.currentColor.color);
    return game.currentColor.color == color;
  },
  secondsToNextColor: 5,
  timer: setInterval(gameIntervalFn, 1000),
  winnerExists: false

};

function gameIntervalFn () {
  if(game.secondsToNextColor === 0){
    game.secondsToNextColor = 5;
    game.winnerExists = false;
  }
  game.secondsToNextColor--;
  for(var room in io.sockets.adapter.rooms){
    io.sockets.in(room).emit('timer', {seconds: game.secondsToNextColor});
  }
  if (game.secondsToNextColor === 0) {
    console.log('End timer');
    clearInterval(game.timer);
    game.setColor();
  }
};

io.on('connection', function(socket){

	socket.on('join:room', function(data){
		var room_name = data.room_name;
		socket.join(room_name);
    socket.emit('timer', {seconds:game.secondsToNextColor});
    socket.emit('newColor', game.currentColor);
	});


	socket.on('leave:room', function(msg){
		msg.text = msg.user + ' has left the room';
		socket.leave(msg.room);
		socket.in(msg.room).emit('message', msg);
	});


	socket.on('send:message', function(msg){
		socket.in(msg.room).emit('message', msg);
	});

  socket.on('send:color', function(msg){
    console.log('send:color', msg);
    if(game.isCorrectColor(msg.color) && !game.winnerExists){
      console.log('winner', msg);
      game.winnerExists = true;
      io.to(msg.room).emit('winner', {user:msg.current_user});
      clearInterval(game.timer);
      game.timer = setInterval(gameIntervalFn, 1000);
    }
  });

  socket.on('send:timer', function(msg){
    console.log('send:timer ', msg);
    game.secondsToNextColor = msg.seconds;
    socket.in(msg.room).emit('timer', {seconds: game.secondsToNextColor});
  });


});
