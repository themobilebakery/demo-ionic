(function(){
	angular.module('starter')
	.controller('RoomController', ['$scope', '$state', 'localStorageService', 'SocketService', 'moment', '$ionicScrollDelegate','$timeout', RoomController]);

	function RoomController($scope, $state, localStorageService, SocketService, moment, $ionicScrollDelegate, $timeout){

		var me = this;

		me.messages = [];

		$scope.humanize = function(timestamp){
			return moment(timestamp).fromNow();
		};

		me.current_room = localStorageService.get('room');

		var current_user = localStorageService.get('username');

    me.current_user = current_user;

		$scope.isNotCurrentUser = function(user){

			if(current_user != user){
				return 'not-current-user';
			}
			return 'current-user';
		};


		$scope.sendTextMessage = function(){

			var msg = {
				'room': me.current_room,
				'user': current_user,
				'text': me.message,
				'time': moment()
			};


			me.messages.push(msg);
			$ionicScrollDelegate.scrollBottom();

			me.message = '';

			SocketService.emit('send:message', msg);
		};


		$scope.leaveRoom = function(){

			var msg = {
				'user': current_user,
				'room': me.current_room,
				'time': moment()
			};

			SocketService.emit('leave:room', msg);
			$state.go('rooms');

		};
    //////////////////
    //COLOR GAME STUFF
    //////////////////

    me.game = {
      secondsToNextColor: 5,
      sendColor: function(user,color,room){
        console.log('Send color', room);
        SocketService.emit('send:color', {'current_user':user, 'color': color, 'room':room});
      },

      colorOptions: [
        {text: 'Red' , color: 'red'},
        {text: 'Blue' , color: 'blue'},
        {text: 'Green' , color: 'green'}
      ],
      notifyCorrect: function(){
        me.game.correctAnswer = true;
        $timeout(function(){
          me.game.correctAnswer = false;
        }, 1000)
      },
      correctAnswer: false
    };

    //////////////////
    //SOCKET LISTENERS
    //////////////////

		SocketService.on('message', function(msg){
      console.log('Message',msg);
			me.messages.push(msg);
			$ionicScrollDelegate.scrollBottom();
		});

    SocketService.on('newColor', function(msg){
      console.log('color', msg);
      me.game.currentColor = msg;
    });

    SocketService.on('timer', function(msg){
      console.log('timer', msg);
      me.game.secondsToNextColor = msg.seconds;
    });

    SocketService.on('winner', function(msg){
      var winnerMsg = {
        'room': me.current_room,
        'user': msg.user,
        'text': msg.user+' Wins! <3',
        'time': moment()
      };
      me.messages.push(winnerMsg);
//      SocketService.emit('send:message', winnerMsg);


      console.log('msg.user', msg.user);
      console.log('me.current_user', me.current_user);
      if(msg.user == me.current_user){
        me.game.notifyCorrect();
      }

    });

	}

})();
